﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdParser
{
    class Program
    {
        static void Main(string[] args)
        {
            //You may need to delete the header names in the file so that the first line in the file is the start of the data
            //You also need to text to columns (in excel) the patches first so that each individual patch is in a column of its own.
            string filePath = @"C:\Users\jkrull\2019Q1Q2LowesAds.csv";
            //This is set up for the file columns to be - Vehicle, Advertise Date, Page Type, Block Type, Item # Item Desc, Promotion Type, Patches. See the example file in this project.

            var AdsCSVText = new StringBuilder();
            //Watch the columns! All the columns from the procedure are added to avoid errors. Make sure the data matches the column! You may need to remove or rearrage columns in the source file
            AdsCSVText.Append(String.Format("Item,Patch,DROP_DATE,GOODTHRU_DATE,DES_TXT,PAGE_NUMBER,PRODUCT_GROUP,SALE_PRICE,UNIT_SALES_FORCAST,ADV_ITEM_QTY,PERCENT_OFF,MEDIA_TYPE{0}", Environment.NewLine));

            StreamReader sr = new StreamReader(filePath);
            while (!sr.EndOfStream)
            {
                string[] Line = sr.ReadLine().Split(',');

                string[] dates = Line[1].Split(new char[] { '-' }, 2);

                    for (int i = 8; i < Line.Length; i++)//For patch columns
                    {
                    
                        if (!String.IsNullOrEmpty(Line[i]))
                        {
                            AdsCSVText.Append(Line[4].ToString() + "," + Line[i].ToString().Trim() + "," + dates[0].ToString().Trim() + "," + dates[1].ToString().Trim() + "," + Line[0].ToString() + ",,,,,,," + Environment.NewLine);
                        }
                    }    
                }


            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\" + "LowesAds_new.csv"))
            {
                outputFile.WriteLine(AdsCSVText);
                outputFile.Close();
            }


            Console.WriteLine("Complete! Look here: " + mydocpath);
            Console.WriteLine("Press enter to close");

            Console.ReadLine();
        }

    }
}
